## Description

A repository to elevate GitLab features such as [Scoped Labels](https://docs.gitlab.com/ee/user/project/labels.html#scoped-labels) and [Related Issues](https://docs.gitlab.com/ee/user/project/issues/related_issues.html) to use GitLab as an organizational work for all (most) SU work (and in a transparent manner, whenever possible).

---

<div align="center">
<p>
    <a href="https://benchling.com/organizations/acubesat/">Benchling 🎐🧬</a> &bull;
    <a href="https://gitlab.com/acubesat/documentation/cdr-public/-/blob/master/DDJF/DDJF_PL.pdf?expanded=true&viewer=rich">DDJF_PL 📚🧪</a> &bull;
    <a href="https://spacedot.gr/">SpaceDot 🌌🪐</a> &bull;
    <a href="https://acubesat.spacedot.gr/">AcubeSAT 🛰️🌎</a>
</p>
</div>

---

## Table of Contents

<details>
<summary>Click to expand</summary>

[[_TOC_]]

</details>

## Features

### Milestones

You can find our milestones [here](https://gitlab.com/groups/acubesat/su/-/milestones). There are four milestones, each for MRR-related development relating to a specific sub-area of focus in SU (subject to change soon):
- https://gitlab.com/groups/acubesat/su/-/milestones/3
- https://gitlab.com/groups/acubesat/su/-/milestones/4
- https://gitlab.com/groups/acubesat/su/-/milestones/6
- https://gitlab.com/groups/acubesat/su/-/milestones/5

There are some more milestones, with `[SW]` as prefix in their title. These are specifically related to the mission software, and for more info you can click [here](https://gitlab.com/acubesat/su/on-board-software/organizational).

### Epics

You can find our epics [here](https://gitlab.com/groups/acubesat/su/-/epics?state=opened&page=1&sort=start_date_desc). This is WIP and subject to change soon.

### Roadmap

You can find our roadmap [here](https://gitlab.com/groups/acubesat/su/-/roadmap?state=opened&sort=end_date_asc&layout=WEEKS&timeframe_range_type=CURRENT_QUARTER&progress=WEIGHT&show_progress=true&show_milestones=true&milestones_type=ALL). This is WIP and subject to change soon.

### Labels

We use labels for characterizing and categorizing all tasks. You can take a look at all the labels and their descriptions by clicking [here](https://gitlab.com/acubesat/su/organizational/-/labels).

#### Priority Labels

These labels are [`prioritized`](https://docs.gitlab.com/ee/user/project/labels.html#label-priority), with the order being the one below.
1.  ~"Priority::Urgent" | `Task that must be dealed with prior to anything else`
2.  ~"Priority::High" | `High priority task`
3.  ~"Priority::Medium" | `Medium priority task`
4.  ~"Priority::Low" | `Low priority task` 

#### Pending Labels

These labels are [`prioritized`](https://docs.gitlab.com/ee/user/project/labels.html#label-priority), with the order being the one below. Only use these in combination with the `Status::On Hold` label mentioned below.
* ~"Pending::Cross-Subsystem" | `Task currently on hold, waiting on work or feedback from another subsystem`
* ~"Pending::Review" | `Task currently on hold, waiting on someone to review and approve`
* ~"Pending::Answer" | `Task currently on hold, waiting on a third-party answer, usually in the form of an email `
* ~"Pending::Meeting" | `Task currently on hold, waiting on a meeting to be carried out, since we can't go through without`
* ~"Pending::Discussion" | `Task currently on hold, waiting on discussion among the team members (and not only)`

#### Status Labels

Each task created should have a `Status::xyz` label.

* ~"Status::Up For Grabs" | `Tasks that have no assignees (yet) and are available for anyone in the subsystem to uptake`
* ~"Status::Help Needed" | `Tasks where the assignee is looking for volunteers to aid in their completion`
* ~"Status::Scheduled" | `Task currently on hold, work scheduled to resume`
* ~"Status::In Progress" | `Task currently being worked on`
* ~"Status::On Hold" | `Task currently on hold, something might be pending`
* ~"Status::Abandoned" | `Tasks that were not completed`

### Boards

#### Kanban Board

Click [here](https://gitlab.com/groups/acubesat/su/-/boards/1872723) to visit our [issue board](https://docs.gitlab.com/ee/user/project/issue_board.html) that functions as a [`Kanban` board](https://en.wikipedia.org/wiki/Kanban_(development)).
Here, the issues (i.e. tasks) are in the form of movable cards. You will find some columns, each corresponding to a `Status::xyz` label.
You can move a task card between columns, and the status (i.e. the label) will also change automatically.

#### Milestones Board

Click [here](https://gitlab.com/groups/acubesat/su/-/boards/4215299) to visit our `Milestones` board.

#### Assignees Board

Click [here](https://gitlab.com/groups/acubesat/su/-/boards/4340087) to visit our `Assignees` board. Here you can find all SU issues grouped according to whom they're assigned to.

### Task Template

We use an [issue template](https://docs.gitlab.com/ee/user/project/description_templates.html#creating-issue-templates), to facilitate creating a new task, and also ensure a basic level of consistency is maintained across issues. The template is currently bare-bones, and you can take a look at it by clicking [here](https://gitlab.com/acubesat/su/organizational/-/blob/master/.gitlab/issue_templates/task_template.md).

## Proposed Workflow

1. Check if any `Priority::Urgent` or `Status::Help Needed` issues exist, and whether you can contribute in their completing. Click [here](https://gitlab.com/groups/acubesat/su/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Priority%3A%3AUrgent&label_name[]=Status%3A%3AHelp%20Needed) to check
2. To see what tasks have been currently assigned to you, visit this link `https://gitlab.com/groups/acubesat/su/-/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=your_username_here`. Make sure to replace `your_username_here` with your username, e.g. `moonrunes`.
3. Tip: you can [subscribe to various labels](https://docs.gitlab.com/ee/user/project/labels.html#subscribing-to-labels) to make sure you get notifications accordingly

### Creating a Task

1. Go to the respective repository, and create an issue following [these guidelines](https://www.tutorialspoint.com/gitlab/gitlab_create_issue.htm) (Note: the `project` will reside in a link `https://gitlab.com/acubesat/su/project_name`)
2. Fill in a short, descriptive `Title`
3. Select the `task_template` option from the `Description` drop-down
4. Make sure to follow the `task_template` guidelines
5. ???
6. _**Profit**_

### Managing a Task

What you can do to update a task:
* Add/remove/change its [label(s)](#labels) (you can also use the [Kanban Board](#kanban-board) for this)
* Update the due date
* Add/remove/change its [assignees](https://docs.gitlab.com/ee/user/project/issues/)
* Leave comments (make sure to use threads whenever possible)
* Be awesome <3
